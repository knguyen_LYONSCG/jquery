$(function () {
    $("#billing-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            
           full_name: {
                required: true,
                nowhitespace: true,
                lettersonly: true,
                
            },

            
            street1: {
                required: true
            },
            
            state: {
                required: true
            },
            
            city: {
                required: true,
                lettersonly: true,     
            },
            
            state: {
                required: true
                 
            },
            
            zip: {
                required: true,
                zipcodeUS: true,
                
                
            },
            
            phone: {
                required: true,
                phoneUS: true
            },
            
            cardName: {
                required: true,
                lettersonly: true,
                minlength: 2
            },
            
            credit_card: {
                required: true,
                creditcard: true,
                creditcardtype: true
            },
            
            month: {
                required: true
            },
            
            year: {
                required: true
            },
            
            cvv: {
                required: true,
                minlength: 3,
                maxlength: 4
            }
            
            
        },
        
        messages: {
            email: {
                required: "Please enter an e-mail address.",
                email: "Please enter a <em>valid</em> e-mail."
            }
        }      
        
    });
});

/* ZIPCODE */ 
//$(function(){
//$('#zipcode').keyup(function(e) {
//    var zipcode = $(this).val();
//    
//    if (zipcode.length === 5 && $.isNumeric(zipcode)) {
//        var requestURL = 'http://ziptasticapi.com/' + zipcode + 
//            '?callback=?';
//        $.getJSON(requestURL, null, function(data) {
//            console.log(data);
//            
//            if (data.city) $('#city').val(data.city);
//            if (data.state) $('#state').val(data.state);
//        });
//    }
//});
//    });


//$.validator.addMethod( "zipcodeUS", function( value, element ) {
//	return this.optional( element ) || /^\d{5}(-\d{4})?$/.test( value );
//}, "The specified US ZIP Code is invalid" );
//


/* Input Mask - Phone */ 

$(function(){
  
  $("#phone_id").mask("(999) 999-9999");


  $("#phone_id").on("blur", function() {
      var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

      if( last.length == 5 ) {
          var move = $(this).val().substr( $(this).val().indexOf("-") + 1, 1 );

          var lastfour = last.substr(1,4);

          var first = $(this).val().substr( 0, 9 );

          $(this).val( first + move + '-' + lastfour );
      }
  });
}); 






